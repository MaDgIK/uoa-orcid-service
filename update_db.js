//version compatibility: 1.0.0-SNAPSHOT
print("here");

function setUserTokenCreationDate(){
  print("\n\n setUserTokenCreationDate \n\n");

  works = db.work.aggregate(
    [
      { $sort: { orcid: 1, creationDate: 1 } },
      {
        $group:
          {
            _id: "$orcid",
            creationDate: { $first: "$creationDate" }
          }
      }
    ]
  );

  while (works.hasNext()) {
    var work = works.next();
    print("\n\n");
    print("DATE OF FIRST WORK FOR ORCID: ",tojson(work));
    userTokens = db.userTokens.find({"orcid": work['_id']}).map(function (userToken) { return userToken; });

    for(var i=0; i<userTokens.length; i++) {
      var userToken = userTokens[i];
      // print("OLD userToken: ",tojson(userToken));

      userToken['creationDate'] = work['creationDate'];

      print(tojson(userToken));
      db.userTokens.save(userToken);
    }
  }

}

function setDashboardInWorks() {
  print("\n\n setDashboardInWorks \n\n");
  var works = db.work.find().map(function (work) { return work; });

  for(var i=0; i<works.length; i++) {
    var current_work = works[i];
    current_work['dashboard'] = "production_explore";
    db.work.save(current_work);
  }
}

use openaire_orcid;

// 29-03-2021 - 20-04-2021
// setUserTokenCreationDate();
// setDashboardInWorks();