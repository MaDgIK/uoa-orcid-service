package eu.dnetlib.uoaorcidservice.services;

import com.google.gson.Gson;
import eu.dnetlib.uoaorcidservice.dao.UserTokensDAO;
import eu.dnetlib.uoaorcidservice.entities.UserTokens;
import eu.dnetlib.uoaorcidservice.handlers.utils.AESUtils;
import eu.dnetlib.uoaorcidservice.handlers.utils.RolesUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;

@Service
public class UserTokensService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private UserTokensDAO userTokensDAO;

    @Autowired
    private RolesUtils rolesUtils;

    public List<UserTokens> getAllUserTokens() {
        return userTokensDAO.findAll();
    }

    public String getUserAccessToken(String orcid) throws BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IOException {
        log.debug("getUserAccessToken: orcid="+orcid);

//        String aaiId = "3";
        String aaiId = rolesUtils.getAaiId();
        UserTokens userTokens = userTokensDAO.findByAaiId(aaiId);
        if(userTokens == null) {
            return null;
        }

        userTokens = decryptTokens(userTokens);

        log.debug("userTokens.getAccessToken(): "+userTokens.getAccessToken());

        return userTokens.getAccessToken();
    }

    public String getCurrentUserOrcid() {
        log.debug("getCurrentUserOrcid");

//        String aaiId = "1";
        String aaiId = rolesUtils.getAaiId();
        UserTokens userTokens = userTokensDAO.findByAaiId(aaiId);
        if(userTokens == null) {
            return null;
        }

        log.debug("userTokens.getOrcidId(): "+userTokens.getOrcid());

        return userTokens.getOrcid();
    }

    public UserTokens getUserTokens() throws BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IOException {
        log.debug("getUserTokens");
        String aaiId = rolesUtils.getAaiId();
        UserTokens userTokens = userTokensDAO.findByAaiId(aaiId);
        return decryptTokens(userTokens);
    }

    public UserTokens json2UserTokens(String json) {
        log.debug("json2UserTokens: "+json);

        if (json == null) {
            return null;
        }

        BufferedReader br = new BufferedReader(new StringReader(json));
        //convert the json string back to object
        Gson gson = new Gson();
        UserTokens userTokens = null;
        try {
            userTokens = gson.fromJson(br, UserTokens.class);
        } catch (Exception e) {
            log.debug("Error in parsing json response. Given json is : " + json, e);
        }

        return userTokens;
    }

    public void saveUserTokens(UserTokens userTokens) throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException {
        userTokens.setCreationDate(new Date());
        userTokens.setAaiId(rolesUtils.getAaiId());
        userTokens = encryptTokens(userTokens);

        userTokensDAO.save(userTokens);
    }

    public UserTokens encryptTokens(UserTokens userTokens) throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException {
//        String password = "mypassword";
//        String salt = "034131145430079";//userTokens.getAaiId();
//        IvParameterSpec ivParameterSpec = AESUtils.generateIv();
//        SecretKey key = AESUtils.getKeyFromPassword(password,salt);
//
//        log.debug("password: "+password);
//        log.debug("salt: "+salt);

//        String cipherText = AESUtils.encryptPasswordBased("test0", key, ivParameterSpec);

        String cipherText = AESUtils.encryptPasswordBased(userTokens.getAccessToken(), userTokens.getAaiId());
        userTokens.setAccessToken(cipherText);
        cipherText = AESUtils.encryptPasswordBased(userTokens.getRefreshToken(), userTokens.getAaiId());
        userTokens.setRefreshToken(cipherText);
//        String decryptedCipherText = AESUtils.decryptPasswordBased(cipherText, key, ivParameterSpec);

        return userTokens;
    }

    public UserTokens decryptTokens(UserTokens userTokens) throws InvalidKeySpecException, NoSuchAlgorithmException, IllegalBlockSizeException, InvalidKeyException, BadPaddingException, InvalidAlgorithmParameterException, NoSuchPaddingException, IOException {
        if(userTokens == null) {
            return null;
        }
//        String password = "mypassword";
//        String salt = "034131145430079";//userTokens.getAaiId();
//        IvParameterSpec ivParameterSpec = AESUtils.generateIv();
//        SecretKey key = AESUtils.getKeyFromPassword(password,salt);

//        log.debug("password: "+password);
//        log.debug("salt: "+salt);

        String token = AESUtils.decryptPasswordBased(userTokens.getAccessToken(), userTokens.getAaiId());
        userTokens.setAccessToken(token);
        token = AESUtils.decryptPasswordBased(userTokens.getRefreshToken(), userTokens.getAaiId());
        userTokens.setRefreshToken(token);


//        String plainText = "www.baeldung.com";
//        String password2 = "mypassword";
//        String salt2 = "034131145430079";
//        IvParameterSpec ivParameterSpec2 = AESUtils.generateIv();
//        SecretKey key2 = AESUtils.getKeyFromPassword(password2,salt2);
//        String cipherText = AESUtils.encryptPasswordBased(plainText, );
//        IvParameterSpec ivParameterSpec3 = AESUtils.generateIv();
//        SecretKey key3 = AESUtils.getKeyFromPassword(password2,salt2);
//        String decryptedCipherText = AESUtils.decryptPasswordBased(cipherText, key3);
//       log.debug("test decrypted text: "+decryptedCipherText);

        return userTokens;
    }

    public UserTokens getUserTokensByAai(String aaiId) throws BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IOException {
        log.debug("getUserTokens");
        UserTokens userTokens = userTokensDAO.findByAaiId(aaiId);
        return decryptTokens(userTokens);
    }

    public UserTokens getEncryptedUserTokensByAai(String aaiId) {
        log.debug("getEncryptedUserTokensByAai");
        return userTokensDAO.findByAaiId(aaiId);
    }
}
