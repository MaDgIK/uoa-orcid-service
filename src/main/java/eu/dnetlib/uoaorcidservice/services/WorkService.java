package eu.dnetlib.uoaorcidservice.services;

import eu.dnetlib.uoaorcidservice.dao.WorkDAO;
import eu.dnetlib.uoaorcidservice.entities.Work;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WorkService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private WorkDAO workDAO;

    public List<Work> getAllWorks() {
        return workDAO.findAll();
    }

    public List<Work> getWorks(String openaireId, String[] pids, String orcid) {
//        return workDAO.findByPidsContainingAndOrcid(pid, orcid);
        if(pids != null) {
            return workDAO.findByPidsInAndOrcid(pids, orcid);
        }
        return workDAO.findByOpenaireIdInAndOrcid(openaireId, orcid);
    }

    public void saveWork(Work work) {
        log.debug("Save in DB work with pids: "+work.getPids() + " & openaireId: " + work.getOpenaireId() + "- for ORCID iD: "+work.getOrcid());
        workDAO.save(work);
    }

    public void deleteWork(String putCode) {
        log.debug("Delete from DB work with putCode: "+putCode);
        workDAO.deleteByPutCode(putCode);
    }

    public List<Work> getLocalWorks(String orcid) {
        return workDAO.findByOrcidOrderByCreationDateDesc(orcid);
//        return workDAO.findByOrcid(orcid);
    }

    public Work getLocalWorkByPutCode(String puutCode) {
        return workDAO.findByPutCode(puutCode);
    }
}
