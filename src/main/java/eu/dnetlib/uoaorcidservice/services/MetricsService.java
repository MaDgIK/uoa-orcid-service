package eu.dnetlib.uoaorcidservice.services;

import eu.dnetlib.uoaorcidservice.dao.MetricsDAO;
import eu.dnetlib.uoaorcidservice.dao.customDAOs.MongoDBUserTokensDAOCustom;
import eu.dnetlib.uoaorcidservice.dao.customDAOs.MongoDBWorkDAOCustom;
import eu.dnetlib.uoaorcidservice.entities.Metrics;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.*;


@EnableScheduling
@Service
public class MetricsService {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    @Qualifier("mongoDBWorkDAO")
    private MongoDBWorkDAOCustom workDAO;

    @Autowired
    @Qualifier("mongoDBUserTokensDAO")
    private MongoDBUserTokensDAOCustom userTokensDAO;

    @Autowired
    private MetricsDAO metricsDAO;

    public List<Object> countWorksPerDashboard() {
        return workDAO.worksPerDashboard();
    }

    public List<Object> countWorksPerYear() {
        return workDAO.worksPerYear();
    }

    public List<Object> countWorksPerYearAndMonth() {
        return workDAO.worksPerYearAndMonth();
    }

    public List<Object> countWorksPerOrcid() {
        return workDAO.worksPerOrcid();
    }

    public List<Object> countUniqueUsersWithLinksPerMonth() {
        return workDAO.uniqueUsersWithLinksPerMonth();
    }

    public List<Object> countUniqueUsersWithLinksPerYear() {
        return workDAO.uniqueUsersWithLinksPerYear();
    }

    public List<Object> countNewUsersPerMonth() {
        return userTokensDAO.newUsersPerMonth();
    }

    public List<Object> countNewUsersPerYear() {
        return userTokensDAO.newUsersPerYear();
    }

    public List<Object> countTokensPerOrcid() {
        return userTokensDAO.tokensPerOrcid();
    }

    public List<Object> countTotalUniqueUsers() {
        return userTokensDAO.totalUniqueUsers();
    }

    public List<Object> countTotalWorks() {
        return workDAO.totalWorks();
    }


    // every day at midnight
    @Scheduled(cron = "0 0 0 * * ?")
//    // every 5 mins for testing
//    @Scheduled(cron = "0 0/5 * * * *")
    public Metrics calculateMetrics() {
        log.info("Calculate metrics and add them in DB");

//        Optional<Metrics> oldMetrics = metricsDAO.findById("current");
//        if(oldMetrics.isPresent()) {
//            oldMetrics.get().setId("previous");
//            metricsDAO.save(oldMetrics.get());
//        }

        Metrics metrics = new Metrics();
        metrics.setId("current");
        List<Object> totalWorks = countTotalWorks();
        if(totalWorks != null && totalWorks.get(0) != null) {
            Map<String, Integer> works = (HashMap<String, Integer>) totalWorks.get(0);
            metrics.setTotal_works(works.get("works"));
        } else {
            metrics.setTotal_works(-1);
            return metrics;
        }
        List<Object> totalUsers = countTotalUniqueUsers();
        if(totalUsers != null && totalUsers.get(0) != null) {
            Map<String, Integer> users = (HashMap<String, Integer>) totalUsers.get(0);
            metrics.setTotal_users(users.get("users"));
        } else {
            metrics.setTotal_users(-1);
            return metrics;
        }
//        metrics.setWorks_per_month(countWorksPerYearAndMonth());
        List<Object> works_per_dashboard = countWorksPerDashboard();
        for(Object dashboardWorksObj : works_per_dashboard) {
            Map<String, Object> dashboardWorks = (HashMap<String, Object>) dashboardWorksObj;
            String[] dashboard_elements = ((String) dashboardWorks.get("dashboard")).split("_", 2);
            dashboardWorks.put("environment", dashboard_elements[0]);
            dashboardWorks.put("dashboard", dashboard_elements[1]);
//            if(dashboard_elements.length > 2) {
//                dashboardWorks.put("dashboardName", dashboard_elements[2]);
//            }
        }
        metrics.setWorks_per_dashboard(works_per_dashboard);
        metrics.setDate(new Date());

        metricsDAO.save(metrics);

//        Total works linked: http://duffy.di.uoa.gr:8080/uoa-orcid-service/report/totalWorks
//        Total users: (unique/ per orcid): http://duffy.di.uoa.gr:8080/uoa-orcid-service/report/totalUniqueUsers
//        Works linked per month: http://duffy.di.uoa.gr:8080/uoa-orcid-service/report/worksPerYearAndMonth (not needed?)
//        Works linked per portal (not only explore): http://duffy.di.uoa.gr:8080/uoa-orcid-service/report/worksPerDashboard
        return metrics;
    }

    public String getMetrics() {
//        # TYPE aai_registered_users_total gauge
//        aai_registered_users_total 50
//# TYPE aai_logins_total counter
//        aai_logins_total 1742
//# TYPE aai_api_requests_total counter
//        aai_api_requests_total 0
//        aai_last_metrics_updater_run_timestamp_seconds 1619157852

        Optional<Metrics> metrics_optional = metricsDAO.findById("current");
        if(!metrics_optional.isPresent()) {
            return null;
        }
        Metrics metrics = metrics_optional.get();
        String response = "";

//        response += "# TYPE explore_orcid_works_total gauge\n";
//        response += "explore_orcid_works_total "+metrics.getTotal_works() + "\n";

        response += "# TYPE orcid_users gauge\n";
        response += "orcid_users "+metrics.getTotal_users() + "\n";


//        if(metrics.getWorks_per_month() != null) {
//            for(Object monthlyWorksObj : metrics.getWorks_per_month()) {
//                Map<String, Integer> monthlyWorks = (HashMap<String, Integer>) monthlyWorksObj;
//                response += "# TYPE orcid_total_works_"+monthlyWorks.get("month")+"_"+monthlyWorks.get("year")+" counter\n";
//                response += "orcid_total_works_"+monthlyWorks.get("month")+"_"+monthlyWorks.get("year")+" "+monthlyWorks.get("works") + "\n";
//
//            }
//        }

        if(metrics.getWorks_per_dashboard() != null) {
            // 2nd approach
//            Map<String, Integer> worksPerEnvironment = new HashMap<>();
            // 3rd approach
            Map<String, Integer> worksPerDashboard = new HashMap<>();
            // 1st approach
//            response += "# TYPE orcid_works gauge\n";

            for(Object dashboardWorksObj : metrics.getWorks_per_dashboard()) {
                Map<String, Object> dashboardWorks = (HashMap<String, Object>) dashboardWorksObj;
                // 1st approach - e.g. orcid_works{envoronment="production" portal="explore"} 10
//                response += "orcid_works{environment=\""+dashboardWorks.get("environment")+"\" portal=\""+dashboardWorks.get("dashboard")+"\"}"+" "+dashboardWorks.get("works") + "\n";

                // 2nd approach - e.g. explore_orcid_works_total{envoronment="production"} 10
//                if(worksPerEnvironment.containsKey(dashboardWorks.get("environment"))) {
//                    int worksSoFar = worksPerEnvironment.get(dashboardWorks.get("environment"));
//                    worksPerEnvironment.put((String)dashboardWorks.get("environment"), (Integer) dashboardWorks.get("works") + worksSoFar);
//                } else {
//                    worksPerEnvironment.put((String)dashboardWorks.get("environment"), (Integer) dashboardWorks.get("works"));
//                }

                // 3rd approach - e.g. orcid_works{portal="explore"} 10
                if(worksPerDashboard.containsKey(dashboardWorks.get("dashboard"))) {
                    int worksSoFar = worksPerDashboard.get(dashboardWorks.get("dashboard"));
                    worksPerDashboard.put((String)dashboardWorks.get("dashboard"), (Integer) dashboardWorks.get("works") + worksSoFar);
                } else {
                    worksPerDashboard.put((String)dashboardWorks.get("dashboard"), (Integer) dashboardWorks.get("works"));
                }
            }
            // 2nd approach
//            response += "# TYPE explore_orcid_works_total gauge\n";
//            for(Map.Entry<String, Integer> envWorks : worksPerEnvironment.entrySet()) {
//                response += "explore_orcid_works_total{environment=\""+envWorks.getKey()+"\"} "+envWorks.getValue() + "\n";
//            }

            // 3rd approach
            response += "# TYPE orcid_works gauge\n";
            for(Map.Entry<String, Integer> envWorks : worksPerDashboard.entrySet()) {
                response += "orcid_works{portal=\""+envWorks.getKey()+"\"} "+envWorks.getValue() + "\n";
            }
        }

        Instant instant = metrics.getDate().toInstant();
        response += "orcid_last_metrics_updater_run_timestamp_seconds "+instant.getEpochSecond()+"\n";

        return response;
    }
}
