package eu.dnetlib.uoaorcidservice.dao;

import eu.dnetlib.uoaorcidservice.entities.Metrics;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface MongoDBMetricsDAO extends MetricsDAO, MongoRepository<Metrics, String> {

    List<Metrics> findAll();

    Optional<Metrics> findById(String Id);

    Metrics save(Metrics metrics);

    void deleteAll();

    void deleteById(String Id);
}
