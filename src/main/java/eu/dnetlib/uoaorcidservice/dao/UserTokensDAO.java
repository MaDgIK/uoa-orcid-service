package eu.dnetlib.uoaorcidservice.dao;

import eu.dnetlib.uoaorcidservice.entities.UserTokens;

import java.util.List;

public interface UserTokensDAO {

    List<UserTokens> findAll();

    UserTokens findByAaiId(String AaiId);

    UserTokens findByOrcid(String Orcid);

    UserTokens save(UserTokens userTokens);

    void deleteAll();

    void deleteByAaiId(String AaiId);

    void deleteByOrcid(String Orcid);
}
