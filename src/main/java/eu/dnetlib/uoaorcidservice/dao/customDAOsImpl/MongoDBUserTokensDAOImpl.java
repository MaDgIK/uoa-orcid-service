package eu.dnetlib.uoaorcidservice.dao.customDAOsImpl;

import com.mongodb.BasicDBObject;
import eu.dnetlib.uoaorcidservice.dao.customDAOs.MongoDBUserTokensDAOCustom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.BSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.DateOperators;

import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregationOptions;

public class MongoDBUserTokensDAOImpl implements MongoDBUserTokensDAOCustom {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final MongoTemplate mongoTemplate;

    @Autowired
    public MongoDBUserTokensDAOImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


    //    monthly  new users grant openaire
//    db.userTokens.aggregate([{"$group":{  "_id":{    "date":{$dateToString: { format: "%Y-%m", date: "$creationDate" }},    orcid: "$orcid"   },  "count":{"$sum":1}}},{"$group":{  "_id":"$_id.date",  "count":{"$sum":1}}}, {$sort : { _id: 1 }} ])
    @Override
    public List<Object> newUsersPerMonth() {
        Aggregation agg = newAggregation(
                project("orcid")
                        .and(DateOperators.dateOf("creationDate").month()).as("month")
                        .and(DateOperators.dateOf("creationDate").year()).as("year"),
                group("month", "year", "orcid")
                        .count().as("orcid_works"),
                group("month", "year")
                        .count().as("works"),
                project("works")
                        .andInclude("month", "year")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "userTokens", Object.class);
        return this.mapResults(results);
    }

    //    yearly  new users grant openaire
//    db.userTokens.aggregate([{"$group":{  "_id":{    "date":{$dateToString: { format: "%Y", date: "$creationDate" }},    orcid: "$orcid"   },  "count":{"$sum":1}}},{"$group":{  "_id":"$_id.date",  "count":{"$sum":1}}}, {$sort : { _id: 1 }} ])
    @Override
    public List<Object> newUsersPerYear() {
        Aggregation agg = newAggregation(
                project("orcid")
                        .and(DateOperators.dateOf("creationDate").year()).as("year"),
                group("year", "orcid")
                        .count().as("orcid_works"),
                group("year")
                        .count().as("works"),
                project("works")
                        .andInclude("year")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "userTokens", Object.class);
        return this.mapResults(results);
    }


    @Override
    public List<Object> tokensPerOrcid() {
        Aggregation agg = newAggregation(
                group("orcid")
                        .count().as("tokens"),
                project("tokens").andInclude("orcid"),
                sort(Sort.Direction.DESC, "tokens")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "userTokens", Object.class);
        return this.mapResults(results);
    }

    @Override
    public List<Object> totalUniqueUsers() {
        Aggregation agg = newAggregation(
                group("orcid"),
                count().as("users"),
                project("users")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "userTokens", Object.class);
        return this.mapResults(results);
    }

    // in spring boot 1.5.11 version, getMappedResults() method is working properly, but in 1.5.8 no - different schema in raw response.
    public List<Object> mapResults(AggregationResults<Object> results) {
        List<Object> mappedResult = results.getMappedResults();

//        log.debug("totalUniqueUsers");
//        log.debug(mappedResult);
//        log.debug(results.getRawResults());

        BSONObject rawResults = results.getRawResults();
        if(rawResults!= null && rawResults.containsField("cursor") && (mappedResult == null || mappedResult.isEmpty())) {
            Map<String, Object> cursorResults = (Map<String, Object>) rawResults.get("cursor");
            if(cursorResults != null && cursorResults.containsKey("firstBatch")) {
                mappedResult = (List<Object>) cursorResults.get("firstBatch");
            }
        }

        return mappedResult;
    }
}
