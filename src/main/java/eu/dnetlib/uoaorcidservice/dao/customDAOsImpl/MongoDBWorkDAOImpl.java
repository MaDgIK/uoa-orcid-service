package eu.dnetlib.uoaorcidservice.dao.customDAOsImpl;

import com.mongodb.BasicDBObject;
import eu.dnetlib.uoaorcidservice.dao.customDAOs.MongoDBWorkDAOCustom;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.BSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.*;

import java.util.List;
import java.util.Map;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

public class MongoDBWorkDAOImpl implements MongoDBWorkDAOCustom {
    private final Logger log = LogManager.getLogger(this.getClass());

    private final MongoTemplate mongoTemplate;

    @Autowired
    public MongoDBWorkDAOImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }


//    works per dashboard
//    db.work.aggregate(  { $group : {  _id : "$dashboard",   count: { $sum: 1 } } }, {$sort : { count: -1 }}  );
    @Override
    public List<Object> worksPerDashboard() {

        Aggregation agg = newAggregation(
                group("dashboard")
                        .count().as("works"),
                project("works")
                        .and("dashboard").previousOperation(),
                sort(Sort.Direction.DESC, "works")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);

    }

//    Yearly work sum
//    db.work.aggregate( { $group : { _id : { $dateToString: { format: "%Y", date: "$creationDate" } }, count: { $sum: 1 } } } , {$sort : { _id: 1 }} );
    @Override
    public List<Object> worksPerYear() {
//        Aggregation agg = newAggregation(
////                project( "creationDate", "putCode" )
//
////                project().and("creationDate").as("creationDate").and("putCode").as("_id")
////                project("orcid", "putCode").and("year(creationDate)").as("_id")
//
////                group("year(creationDate)"),
////                project().and("year(creationDate)").previousOperation()
//
//
//                project().and(DateOperators.dateOf("creationDate").year()).as("field1")
//
////                project().andInclude("orcid", "putCode").and("year(creationDate)").previousOperation()
////                        .and("creationDate").previousOperation()
//        );


        Aggregation agg = newAggregation(
                project().and(DateOperators.dateOf("creationDate").year()).as("year"),
                group("year")
                        .count().as("works"),
                project("works")
                        .and("year").previousOperation()
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
    }


//    Monthly work sum
//    db.work.aggregate( { $group : { _id : { $dateToString: { format: "%Y-%m", date: "$creationDate" } }, count: { $sum: 1 } } } , {$sort : { _id: 1 }} );
//    @Override
    public List<Object> worksPerYearAndMonth() {
        Aggregation agg = newAggregation(
                project()
                        .and(DateOperators.dateOf("creationDate").year()).as("year")
                        .and(DateOperators.dateOf("creationDate").month()).as("month"),
                group("month", "year")
                        .count().as("works"),
                project("works")
                        .andInclude("month", "year")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
    }

//    Work Count per orcid
//    db.work.aggregate(  { $group : {  _id : "$orcid",   count: { $sum: 1 } } }, {$sort : { count: -1 }}  );
    @Override
    public List<Object> worksPerOrcid() {
        Aggregation agg = newAggregation(
                group("orcid")
                        .count().as("works"),
                project("works")
                        .andInclude("orcid"),
                sort(Sort.Direction.DESC, "works")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
    }


//    monthly  usage: unique users that linked a work
//    db.work.aggregate([{"$group":{  "_id":{    "date":{$dateToString: { format: "%Y-%m", date: "$creationDate" }},    orcid: "$orcid"   },  "count":{"$sum":1}}},{"$group":{  "_id":"$_id.date",  "count":{"$sum":1}}}, {$sort : { _id: 1 }} ])
    @Override
    public List<Object> uniqueUsersWithLinksPerMonth() {
        Aggregation agg = newAggregation(
                project("orcid")
                        .and(DateOperators.dateOf("creationDate").month()).as("month")
                        .and(DateOperators.dateOf("creationDate").year()).as("year"),
                group("month", "year", "orcid")
                        .count().as("orcid_works"),
                group("month", "year")
                        .count().as("works"),
                project("works")
                        .andInclude("month", "year")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
    }

//    yearly  usage: unique users that linked a work
//    db.work.aggregate([{"$group":{  "_id":{    "date":{$dateToString: { format: "%Y", date: "$creationDate" }},    orcid: "$orcid"   },  "count":{"$sum":1}}},{"$group":{  "_id":"$_id.date",  "count":{"$sum":1}}}, {$sort : { _id: 1 }} ])
    @Override
    public List<Object> uniqueUsersWithLinksPerYear() {
        Aggregation agg = newAggregation(
                project("orcid")
                        .and(DateOperators.dateOf("creationDate").year()).as("year"),
                group("year", "orcid")
                        .count().as("orcid_works"),
                group("year")
                        .count().as("works"),
                project("works")
                        .andInclude("year")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
    }

    @Override
    public List<Object> totalWorks() {
        Aggregation agg = newAggregation(
                count().as("works"),
                project("works")
        );

        AggregationResults<Object> results = mongoTemplate.aggregate(
                agg.withOptions(newAggregationOptions().cursor(new BasicDBObject()).build()),
                "work", Object.class);
        return this.mapResults(results);
//        return mongoTemplate.count(null, "work");
    }

    // in spring boot 1.5.11 version, getMappedResults() method is working properly, but in 1.5.8 no - different schema in raw response.
    public List<Object> mapResults(AggregationResults<Object> results) {
        List<Object> mappedResult = results.getMappedResults();

//        log.debug("totalUniqueUsers");
//        log.debug(mappedResult);
//        log.debug(results.getRawResults());

        BSONObject rawResults = results.getRawResults();
        if(rawResults!= null && rawResults.containsField("cursor") && (mappedResult == null || mappedResult.isEmpty())) {
            Map<String, Object> cursorResults = (Map<String, Object>) rawResults.get("cursor");
            if(cursorResults != null && cursorResults.containsKey("firstBatch")) {
                mappedResult = (List<Object>) cursorResults.get("firstBatch");
            }
        }

        return mappedResult;
    }
}