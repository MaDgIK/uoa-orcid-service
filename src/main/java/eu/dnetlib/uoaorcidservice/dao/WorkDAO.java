package eu.dnetlib.uoaorcidservice.dao;

import eu.dnetlib.uoaorcidservice.entities.Work;

import java.util.List;
import java.util.Optional;

public interface WorkDAO {
    List<Work> findAll();

    Optional<Work> findById(String Id);

//    List<Work> findByPidsContaining(String Pid);

//    Work findByPidsContainingAndOrcid(String Pid, String Orcid);

    List<Work> findByPidsInAndOrcid(String[] Pids, String Orcid);
    List<Work> findByOpenaireIdInAndOrcid(String OpenaireId, String Orcid);

    List<Work> findByOrcidOrderByCreationDateDesc(String Orcid);
    List<Work> findByOrcid(String Orcid);

    Work findByPutCode(String putCode);

    Work save(Work work);

    void deleteAll();

    void deleteById(String Id);

    void deleteByPutCode(String PutCode);
}
