package eu.dnetlib.uoaorcidservice.dao;

import eu.dnetlib.uoaorcidservice.entities.Metrics;

import java.util.List;
import java.util.Optional;

public interface MetricsDAO {
    List<Metrics> findAll();

    Optional<Metrics> findById(String Id);

    Metrics save(Metrics metrics);

    void deleteAll();

    void deleteById(String Id);
}
