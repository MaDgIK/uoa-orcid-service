package eu.dnetlib.uoaorcidservice.dao;

import eu.dnetlib.uoaorcidservice.dao.customDAOs.MongoDBUserTokensDAOCustom;
import eu.dnetlib.uoaorcidservice.entities.UserTokens;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MongoDBUserTokensDAO extends MongoDBUserTokensDAOCustom, UserTokensDAO, MongoRepository<UserTokens, String> {

    List<UserTokens> findAll();

    UserTokens findByAaiId(String AaiId);

    UserTokens findByOrcid(String Orcid);

    UserTokens save(UserTokens userTokens);

    void deleteAll();

    void deleteByAaiId(String AaiId);

    void deleteByOrcid(String Orcid);
}
