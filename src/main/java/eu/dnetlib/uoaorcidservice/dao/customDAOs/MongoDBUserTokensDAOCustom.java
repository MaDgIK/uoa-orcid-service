package eu.dnetlib.uoaorcidservice.dao.customDAOs;

import java.util.List;

public interface MongoDBUserTokensDAOCustom {
    List<Object> newUsersPerMonth();
    List<Object> newUsersPerYear();

    List<Object> tokensPerOrcid();
    List<Object> totalUniqueUsers();
}
