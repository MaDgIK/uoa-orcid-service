package eu.dnetlib.uoaorcidservice.dao.customDAOs;


import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface MongoDBWorkDAOCustom {
    List<Object> worksPerDashboard();
    List<Object> worksPerYear();
    List<Object> worksPerYearAndMonth();
    List<Object> worksPerOrcid();

    List<Object> uniqueUsersWithLinksPerMonth();
    List<Object> uniqueUsersWithLinksPerYear();

    List<Object> totalWorks();
}
