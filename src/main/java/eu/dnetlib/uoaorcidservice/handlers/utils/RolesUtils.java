package eu.dnetlib.uoaorcidservice.handlers.utils;

import eu.dnetlib.uoaauthorizationlibrary.security.AuthorizationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RolesUtils {
    @Autowired
    private AuthorizationService authorizationService;

    private final Logger log = LogManager.getLogger(this.getClass());

    public List<String> getRoles() {
        return authorizationService.getRoles();
    }

    public String getAaiId() {
        return authorizationService.getAaiId();
    }

    public boolean isLoggedIn(List<String> roles) {
        if(roles == null || roles.contains(authorizationService.ANONYMOUS_USER)) {
            return false;
        }
        return true;
    }

}
