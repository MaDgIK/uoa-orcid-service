package eu.dnetlib.uoaorcidservice.handlers;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NO_CONTENT)
public class CommunityNotFoundException extends RuntimeException {
    public CommunityNotFoundException(String message){
            super(message);
    }
}

