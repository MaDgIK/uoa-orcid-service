package eu.dnetlib.uoaorcidservice.handlers;

import com.google.gson.JsonSyntaxException;
import eu.dnetlib.uoaorcidservice.responses.ExceptionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.nio.file.AccessDeniedException;

@ControllerAdvice
public class ExceptionsHandler {
    private final Logger log = LogManager.getLogger(this.getClass());

//    @ExceptionHandler(MissingServletRequestParameterException.class)
//    public ResponseEntity<ExceptionResponse> invalidInput(Exception ex) {
//        ExceptionResponse response = new ExceptionResponse();
//        response.setErrorCode("Validation Error");
//        response.setErrorMessage("Invalid inputs.");
//        response.setErrors(ex.getMessage());
//        log.debug("invalidInput exception");
//
//        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
//    }
//
//    @ExceptionHandler(NullPointerException.class)
//    public ResponseEntity<ExceptionResponse> nullPointerException(Exception ex) {
//        ExceptionResponse response = new ExceptionResponse();
//        response.setErrorCode("Null pointer Exception");
//        response.setErrorMessage("Null pointer Exception");
//        response.setErrors(ex.getMessage());
//        log.debug("nullPointerException exception");
//        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.BAD_REQUEST);
//    }


//    @ExceptionHandler(AccessDeniedException.class)
//    public ResponseEntity<ExceptionResponse> accessDeniedException(Exception ex) {
//        ExceptionResponse response = new ExceptionResponse();
//        response.setErrorCode("Forbidden Exception");
//        response.setErrorMessage("Access Denied Exception");
//        response.setErrors(ex.getMessage());
//        response.setStatus(HttpStatus.FORBIDDEN);
//        log.error("accessDeniedException exception : "+ ex.getMessage());
//        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.FORBIDDEN);
//    }

    @ExceptionHandler(ContentNotFoundException.class)
    public ResponseEntity<ExceptionResponse> contentNotFoundException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("No content found");
        response.setErrorMessage(ex.getMessage());
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.NOT_FOUND);
        log.error("contentNotFound exception: " + response.getErrorCode()+ " - "+response.getErrorMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ForbiddenException.class)
    public ResponseEntity<ExceptionResponse> forbiddenException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Forbidden Exception");
        response.setErrorMessage("Forbidden Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.FORBIDDEN);
        log.error("forbiddenException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(AuthorizationServiceException.class)
    public ResponseEntity<ExceptionResponse> authorizationServiceException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("User not registered");
        response.setErrorMessage("Authorization Service Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.UNAUTHORIZED);
        log.error("authorizationServiceException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(JsonSyntaxException.class)
    public ResponseEntity<ExceptionResponse> jsonSyntaxException(Exception ex) {
        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode("Json syntax is not valid");
        response.setErrorMessage("Json Syntax Exception");
        response.setErrors(ex.getMessage());
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        log.error("jsonSyntaxException exception : "+ ex.getMessage());
        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<ExceptionResponse> exception(Exception ex) {
//        ExceptionResponse response = new ExceptionResponse();
//        response.setErrorCode("Internal server error");
//        response.setErrorMessage("Exception");
//        response.setErrors(ex.getMessage());
//        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
//        log.error("Exception exception : "+ ex.getMessage());
//        return new ResponseEntity<ExceptionResponse>(response, HttpStatus.INTERNAL_SERVER_ERROR);
//    }

}
