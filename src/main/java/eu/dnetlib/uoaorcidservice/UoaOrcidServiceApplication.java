package eu.dnetlib.uoaorcidservice;

import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;
import eu.dnetlib.uoaorcidservice.configuration.GlobalVars;
import eu.dnetlib.uoaorcidservice.configuration.properties.APIProperties;
import eu.dnetlib.uoaorcidservice.configuration.properties.MongoConfig;
//import eu.dnetlib.uoaauthorizationlibrary.configuration.AuthorizationConfiguration;
import eu.dnetlib.uoaorcidservice.configuration.properties.OrcidConfig;
import eu.dnetlib.uoaorcidservice.handlers.utils.AESUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication(scanBasePackages = {"eu.dnetlib.uoaorcidservice"})
@PropertySources({
        @PropertySource("classpath:authorization.properties"),
        @PropertySource("classpath:orcidservice.properties"),
        @PropertySource(value = "classpath:dnet-override.properties", ignoreResourceNotFound = true)
})
@EnableConfigurationProperties({MongoConfig.class, AESUtils.class, OrcidConfig.class, GlobalVars.class, APIProperties.class})
@Import(AuthorizationConfiguration.class)
public class UoaOrcidServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UoaOrcidServiceApplication.class, args);
    }
}
