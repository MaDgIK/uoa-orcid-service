package eu.dnetlib.uoaorcidservice.controllers;

import com.mongodb.BasicDBObject;
import com.mongodb.CommandResult;
import com.mongodb.DBObject;
import eu.dnetlib.uoaorcidservice.configuration.GlobalVars;
import eu.dnetlib.uoaorcidservice.configuration.mongo.MongoConnection;
import eu.dnetlib.uoaorcidservice.configuration.properties.MongoConfig;
import eu.dnetlib.uoaorcidservice.configuration.properties.OrcidConfig;
import eu.dnetlib.uoaorcidservice.handlers.utils.AESUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin(origins = "*")
public class OrcidServiceCheckDeployController {
    private final Logger log = LogManager.getLogger(this.getClass());

    @Autowired
    private MongoConnection mongoConnection;

    @Autowired
    private MongoConfig mongoConfig;

    @Autowired
    private OrcidConfig orcidConfig;

    @Autowired
    private GlobalVars globalVars;

    @Autowired
    private AESUtils aesUtils;

    @RequestMapping(value = {"", "/health_check"}, method = RequestMethod.GET)
    public String hello() {
        log.debug("Hello from uoa-orcid-service!");
        return "Hello from uoa-orcid-service!";
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/health_check/advanced", method = RequestMethod.GET)
    public Map<String, String> checkEverything() {
        Map<String, String> response = new HashMap<>();

        MongoTemplate mt = mongoConnection.getMongoTemplate();
        DBObject ping = new BasicDBObject("ping", "1");
        try {
            CommandResult answer = mt.getDb().command(ping);
            response.put("Mongo try: error", answer.getErrorMessage());
        } catch (Exception e) {
            response.put("Mongo catch: error", e.getMessage());
        }

//        Document ping = new Document("ping", "1");
//        try {
//            Document answer = mt.getDb().runCommand(ping);//.command(ping);
//
//            for(String key : answer.keySet()) {
//                log.debug("answer pair: "+key + ": "+answer.get(key));
//            }
//
//            if (answer != null && answer.get("ok") != null && (Double)answer.get("ok") == 1.0d) {
//                response.put("Mongo try: everything is ok", answer.toString());//.getErrorMessage());
//            } else {
//                response.put("Mongo try: error", answer.toString());//.getErrorMessage());
//            }
//        } catch (MongoTimeoutException e) {
//            response.put("Mongo catch timeout: error", e.getMessage());
//        } catch (Exception e) {
//            response.put("Mongo catch: error", e.getMessage());
//        }

        response.put("orcidservice.mongodb.database", mongoConfig.getDatabase());
        response.put("orcidservice.mongodb.host", mongoConfig.getHost());
        response.put("orcidservice.mongodb.port", mongoConfig.getPort()+"");
        response.put("orcidservice.mongodb.username", mongoConfig.getUsername() == null ? null : "[unexposed value]");
        response.put("orcidservice.mongodb.password", mongoConfig.getPassword() == null ? null : "[unexposed value]");

        response.put("orcidservice.orcid.apiURL", orcidConfig.getApiURL());
        response.put("orcidservice.orcid.tokenURL", orcidConfig.getTokenURL());
        response.put("orcidservice.orcid.clientId", orcidConfig.getClientId() == null ? null : "[unexposed value]");
        response.put("orcidservice.orcid.clientSecret", orcidConfig.getClientSecret() == null ? null : "[unexposed value]");

        response.put("orcidservice.encryption.password", aesUtils.getPassword() == null ? null : "[unexposed value]");

        if(globalVars.date != null) {
            response.put("Date of deploy", globalVars.date.toString());
        }
        if(globalVars.getBuildDate() != null) {
            response.put("Date of build", globalVars.getBuildDate());
        }
        if(globalVars.getVersion() != null) {
            response.put("Version", globalVars.getVersion());
        }
        return response;
    }
}
