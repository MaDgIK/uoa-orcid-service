package eu.dnetlib.uoaorcidservice.controllers;

import eu.dnetlib.uoaorcidservice.configuration.properties.OrcidConfig;
import eu.dnetlib.uoaorcidservice.entities.UserTokens;
import eu.dnetlib.uoaorcidservice.responses.SingleValueWrapperResponse;
import eu.dnetlib.uoaorcidservice.services.UserTokensService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

@RestController
//@RequestMapping("/orcid")
@PreAuthorize("isAuthenticated()")
@CrossOrigin(origins = "*")
public class UserTokensController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Logger orcid_log = LogManager.getLogger("ORCID-"+this.getClass().getName());

    @Autowired
    private OrcidConfig orcidConfig;

    @Autowired
    private UserTokensService userTokensService;

//    @RequestMapping(value = "/tokens", method = RequestMethod.GET)
//    public List<UserTokens> getAllUserTokens() {
//        return userTokensService.getAllUserTokens();
//    }

//    @RequestMapping(value = "/token/access_token", method = RequestMethod.GET)
//    public String getUserAccessTokenByOrcid(@RequestParam String orcid) {
//        return "\""+userTokensService.getUserAccessToken(orcid)+"\"";
//    }

    @RequestMapping(value = "/local/orcidId", method = RequestMethod.GET)
    public SingleValueWrapperResponse<String> getUserOrcidId() throws BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, IOException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        UserTokens userTokens = userTokensService.getUserTokens();
        if(userTokens == null) {
            throw new AuthorizationServiceException("User is not registered");
        }
        String userOrcid = userTokens.getOrcid();
        return new SingleValueWrapperResponse<String>(userOrcid);
    }

    @RequestMapping(value = "/orcid/token/save", method = RequestMethod.GET)
    public SingleValueWrapperResponse<Boolean> saveUserTokens(@RequestParam String code
//            , @RequestParam String redirect_uri
    ) throws BadPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidKeyException, InvalidKeySpecException, IOException {
        log.debug("saveUserTokens: code="+code);

        String url = orcidConfig.getTokenURL();//"https://sandbox.orcid.org/oauth/token";
        String clientId = orcidConfig.getClientId();//"APP-A5M3KTX6NCN67L91";
        String clientSecret = orcidConfig.getClientSecret();//"96b20d71-ae06-4286-bb00-9172536c1ad4";


        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }});
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type","application/x-www-form-urlencoded");
        headers.add("Accept","application/json");

        String inputString =
                "client_id="+clientId
                        +"&client_secret="+clientSecret
                        +"&grant_type=authorization_code"
                        +"&code="+code;
//                        +"&redirect_uri="+redirect_uri;//http://duffy.di.uoa.gr:4300/orcid";

        HttpEntity<String> request = new HttpEntity<>(inputString, headers);
        orcid_log.info("url: "+url);
        orcid_log.info("request: "+request);

        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST, request, String.class);
        if(response.getStatusCode() != HttpStatus.OK) {
            orcid_log.error("User tokens response code is: " + response.getStatusCode());
            orcid_log.error("Unexpected Response: "+response.getBody());
            return new SingleValueWrapperResponse<Boolean>(false);
        } else {
            orcid_log.info("Response: "+response);

            UserTokens userTokens = userTokensService.json2UserTokens(response.getBody().toString());
            userTokensService.saveUserTokens(userTokens);

            return new SingleValueWrapperResponse<Boolean>(true);
//            return "\""+userTokens.getAccessToken()+"\"";
        }
    }

    @RequestMapping(value = "/orcid/personal-details", method = RequestMethod.GET)
    public String getPersonalDetailsFromOrcid() throws Exception {
        log.debug("getPersonalDetailsFromOrcid");

        UserTokens userTokens = userTokensService.getUserTokens();
        if(userTokens == null) {
            throw new AuthorizationServiceException("User is not registered");
        }
        String userOrcid = userTokens.getOrcid();
        String userAccessToken = userTokens.getAccessToken();

        if(userOrcid == null || userAccessToken == null) {
            throw new AuthorizationServiceException("User is not registered");
        }

//        log.debug("Access token: " + userAccessToken);
//        log.debug("User orcid: " + userOrcid);

        String url = orcidConfig.getApiURL()+userOrcid+"/personal-details";

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
            protected boolean hasError(HttpStatus statusCode) {
                return false;
            }
        });
        HttpHeaders headers = new HttpHeaders();
        headers.add("Accept", "application/json");
        headers.add("Authorization", "Bearer " + userAccessToken);
        headers.add("Content-Type", "application/orcid+json");
        HttpEntity<String> request = new HttpEntity<>(headers);

        orcid_log.info("request: "+request);
        orcid_log.info("url: "+url);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
        if (response.getStatusCode() != HttpStatus.OK) {
            orcid_log.error("Getting user details response code is: " + response.getStatusCode());
            orcid_log.error("Unexpected Response: "+response.getBody());

            if(response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
                throw new AuthorizationServiceException("You are not allowed to get personal details");
            } else {
                throw new Exception("Internal server error");
            }
//            return null;
        } else {
            orcid_log.info("response: "+response);
            return response.getBody().toString();
        }
    }


//    @PreAuthorize("isAuthenticated()")
//    @RequestMapping(value = "/local/tokens/decrypt", method = RequestMethod.GET)
//    public UserTokens decryptToken(@RequestParam String aaiId) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, IOException {
//        return userTokensService.getUserTokensByAai(aaiId);
//    }
//
//
//    @PreAuthorize("isAuthenticated()")
//    @RequestMapping(value = "/local/tokens/encrypt", method = RequestMethod.GET)
//    public UserTokens encryptToken(@RequestParam String aaiId) throws NoSuchPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, IOException {
//        UserTokens userTokens = userTokensService.getEncryptedUserTokensByAai(aaiId);
//        return userTokensService.encryptTokens(userTokens);
//    }
}
