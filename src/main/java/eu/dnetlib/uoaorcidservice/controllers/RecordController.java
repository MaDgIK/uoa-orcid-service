//package eu.dnetlib.uoaorcidservice.controllers;
//
//import eu.dnetlib.uoaorcidservice.services.UserTokensService;
//import org.apache.log4j.Logger;
//import org.springframework.http.*;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.client.DefaultResponseErrorHandler;
//import org.springframework.web.client.RestTemplate;
//
//import javax.crypto.BadPaddingException;
//import javax.crypto.IllegalBlockSizeException;
//import javax.crypto.NoSuchPaddingException;
//import java.io.IOException;
//import java.security.InvalidAlgorithmParameterException;
//import java.security.InvalidKeyException;
//import java.security.NoSuchAlgorithmException;
//import java.security.spec.InvalidKeySpecException;
//
//@RestController
//@RequestMapping("/orcid")
//@CrossOrigin(origins = "*")
//public class RecordController {
//    private final Logger log = Logger.getLogger(this.getClass());
//
//    @Autowired
//    UserTokensService userTokensService;
//
//    @RequestMapping(value = {"/record"}, method = RequestMethod.GET)
//    public String getRecord() throws BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, IllegalBlockSizeException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeySpecException, IOException {
//        String testingUserToken = userTokensService.getUserAccessToken(null);
//        String testingUserOrcid = userTokensService.getCurrentUserOrcid();
//
//        String url = "https://api.sandbox.orcid.org/v3.0/"+testingUserOrcid;//+"/record";
//
//        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setErrorHandler(new DefaultResponseErrorHandler(){
//            protected boolean hasError(HttpStatus statusCode) {
//                return false;
//            }});
//        HttpHeaders headers = new HttpHeaders();
//        headers.add("Accept", "application/json");
//        headers.add("Authorization", "Bearer "+testingUserToken);
//        headers.add("Content-Type","application/orcid+json");
//
//        HttpEntity<String> request = new HttpEntity<>(headers);
//        //logger.info(restTemplate.exchange(fooResourceUrl, HttpMethod.GET, request, Object.class));
//        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, request, String.class);
//        if(response.getStatusCode() != HttpStatus.OK) {
//            log.debug("Getting orcid record response code is: " + response.getStatusCode());
//            return null;
//        } else {
//            log.debug(response);
//            return response.getBody().toString();
//        }
//
////        try {
////            URL obj = new URL(url);
////
////            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
////            con.setRequestProperty("Accept", "application/json");
////            con.setRequestProperty("Content-Type", "application/json");
////            con.setRequestProperty("Authorization", "Bearer "+testingUserToken);
////
////            if (con.getResponseCode() != 200) {
////                log.debug("User record response code is: " + con.getResponseCode());
////                return null;
////            }
////            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
////            StringBuilder response = new StringBuilder();
////            String inputLine;
////            while ((inputLine = in.readLine()) != null) {
////                response.append(inputLine).append("\n");
////            }
////            in.close();
////            log.debug(response);
////            return response;
////        } catch (Exception e) {
////            log.error("An error occured while trying to fetch user record ", e);
////            return null;
////        }
//    }
//}