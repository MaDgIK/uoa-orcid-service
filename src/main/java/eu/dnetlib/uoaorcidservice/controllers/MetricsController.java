package eu.dnetlib.uoaorcidservice.controllers;

import eu.dnetlib.uoaorcidservice.services.MetricsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class MetricsController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Logger orcid_log = LogManager.getLogger("ORCID-" + this.getClass().getName());

    @Autowired
    private MetricsService metricsService;


    @RequestMapping(value = "/report/worksPerDashboard", method = RequestMethod.GET)
    public List<Object> countWorksPerDashboard() {
        return metricsService.countWorksPerDashboard();
    }

    @RequestMapping(value = "/report/worksPerYear", method = RequestMethod.GET)
    public List<Object> countWorksPerYear() {
        return metricsService.countWorksPerYear();
    }

    @RequestMapping(value = "/report/worksPerYearAndMonth", method = RequestMethod.GET)
    public List<Object> countWorksPerYearAndMonth() {
        return metricsService.countWorksPerYearAndMonth();
    }

    @RequestMapping(value = "/report/worksPerOrcid", method = RequestMethod.GET)
    public List<Object> countWorksPerOrcid() {
        return metricsService.countWorksPerOrcid();
    }

    @RequestMapping(value = "/report/uniqueUsersWithLinksPerMonth", method = RequestMethod.GET)
    public List<Object> countUniqueUsersWithLinksPerMonth() {
        return metricsService.countUniqueUsersWithLinksPerMonth();
    }

    @RequestMapping(value = "/report/uniqueUsersWithLinksPerYear", method = RequestMethod.GET)
    public List<Object> countUniqueUsersWithLinksPerYear() {
        return metricsService.countUniqueUsersWithLinksPerYear();
    }

    @RequestMapping(value = "/report/newUsersPerMonth", method = RequestMethod.GET)
    public List<Object> countNewUsersPerMonth() {
        return metricsService.countNewUsersPerMonth();
    }

    @RequestMapping(value = "/report/newUsersPerYear", method = RequestMethod.GET)
    public List<Object> countNewUsersPerYear() {
        return metricsService.countNewUsersPerYear();
    }

    @RequestMapping(value = "/report/tokensPerOrcid", method = RequestMethod.GET)
    public List<Object> countTokensPerOrcid() {
        return metricsService.countTokensPerOrcid();
    }

    @RequestMapping(value = "/report/totalUniqueUsers", method = RequestMethod.GET)
    public List<Object> countTotalUniqueUsers() {
        return metricsService.countTotalUniqueUsers();
    }

    @RequestMapping(value = "/report/totalWorks", method = RequestMethod.GET)
    public List<Object> countTotalWorks() {
        return metricsService.countTotalWorks();
    }

    @RequestMapping(value = "/report/metrics", method = RequestMethod.GET, produces={"text/plain"})
    public String getMetrics() {
        return metricsService.getMetrics();
    }

    @PreAuthorize("hasAnyAuthority(@AuthorizationService.PORTAL_ADMIN)")
    @RequestMapping(value = "/report/calc", method = RequestMethod.GET)
    public String calculateMetrics() {
        metricsService.calculateMetrics();
        return "ALL GOOD";
    }
}
