package eu.dnetlib.uoaorcidservice.controllers;
import eu.dnetlib.uoaorcidservice.responses.ExceptionResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

//public class SimpleErrorController {}

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/error")
public class SimpleErrorController implements ErrorController {
    private final Logger log = LogManager.getLogger(this.getClass());
    private final Logger orcid_log = LogManager.getLogger("ORCID-"+this.getClass().getName());

    private final ErrorAttributes errorAttributes;

    @Autowired
    public SimpleErrorController(ErrorAttributes errorAttributes) {
        Assert.notNull(errorAttributes, "ErrorAttributes must not be null");
        this.errorAttributes = errorAttributes;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    @RequestMapping
    public ResponseEntity<ExceptionResponse> error(HttpServletRequest aRequest){
        Map<String, Object> body = getErrorAttributes(aRequest,getTraceParameter(aRequest));
//        String trace = (String) body.get("trace");
//        log.debug(trace);
//        if(trace != null){
//            String[] lines = trace.split("\n\t");
//            log.debug(lines);
//            body.put("trace", lines);
//        }
//        log.debug(body);

//        {timestamp=Mon Jan 04 14:29:25 EET 2021,
//                status=500,
//                error=Internal Server Error,
//                exception=org.springframework.web.client.UnknownHttpStatusCodeException,
//                message=Unknown status code [525] Origin SSL Handshake Error,
//            path=/uoa-orcid-service/orcid/work/save}

        String path = (String)body.get("path");
        if(path.contains("/uoa-orcid-service/orcid")) {
            orcid_log.error(body);
        } else {
            log.error(body);
        }

        Integer status = (Integer)body.get("status");

        HttpStatus statusCode = HttpStatus.INTERNAL_SERVER_ERROR;
        if (status != null) {
            statusCode = HttpStatus.valueOf(status);
        }

        ExceptionResponse response = new ExceptionResponse();
        response.setErrorCode((String)body.get("error"));
        response.setErrorMessage((String)body.get("exception"));
        response.setErrors((String)body.get("message"));
        response.setStatus(statusCode);
//        log.error((String)body.get("exception")+" : "+ (String)body.get("message"));
        return new ResponseEntity<ExceptionResponse>(response, statusCode);

//        return body;
    }

    private boolean getTraceParameter(HttpServletRequest request) {
        String parameter = request.getParameter("trace");
        if (parameter == null) {
            return false;
        }
        return !"false".equals(parameter.toLowerCase());
    }

    private Map<String, Object> getErrorAttributes(HttpServletRequest aRequest, boolean includeStackTrace) {
        RequestAttributes requestAttributes = new ServletRequestAttributes(aRequest);
//        ServletWebRequest requestAttributes = new ServletWebRequest(aRequest);
        return errorAttributes.getErrorAttributes(requestAttributes, includeStackTrace);
    }
}