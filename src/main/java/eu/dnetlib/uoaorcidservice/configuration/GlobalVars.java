package eu.dnetlib.uoaorcidservice.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Date;

@ConfigurationProperties("orcidservice.globalVars")
public class GlobalVars {
    public static Date date = new Date();
    private Date buildDate;
    private String version;

    public String getBuildDate() {
        if(buildDate == null) {
            return null;
        }
        return buildDate.toString();
    }

    public void setBuildDate(Date buildDate) {
        this.buildDate = buildDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
