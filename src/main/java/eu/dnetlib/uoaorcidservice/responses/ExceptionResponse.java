package eu.dnetlib.uoaorcidservice.responses;

import org.springframework.http.HttpStatus;

public class ExceptionResponse {
    private HttpStatus status;
    private String errorCode;
    private String errorMessage;
    private String errors;

    public ExceptionResponse() {}

    public HttpStatus getStatus() { return status; }

    public void setStatus(HttpStatus status) { this.status = status; }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrors() {
        return errors;
    }

    public void setErrors(String errors) {
        this.errors = errors;
    }
}
