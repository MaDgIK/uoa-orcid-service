package eu.dnetlib.uoaorcidservice.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

import java.util.Date;
import java.util.List;

enum Identifier {
    // Do not rename or remove existring values. This may cause problems with already stored values in DB
    current, previous
}

public class Metrics {
    @Id
    @JsonProperty("_id")
    private Identifier id;

    private int total_works;
    private int total_users;
//    private List<Object> works_per_month;
    private List<Object>  works_per_dashboard;
    private Date date;

    public String getId() {
        if(id == null) {
            return null;
        }
        return id.name();
    }

    public void setId(String id) {
        if(id == null) {
            this.id = null;
        } else {
            Identifier identifier = Identifier.valueOf(id);
            this.id = identifier;
        }
    }

    public int getTotal_works() {
        return total_works;
    }

    public void setTotal_works(int total_works) {
        this.total_works = total_works;
    }

    public int getTotal_users() {
        return total_users;
    }

    public void setTotal_users(int total_users) {
        this.total_users = total_users;
    }

//    public List<Object> getWorks_per_month() {
//        return works_per_month;
//    }
//
//    public void setWorks_per_month(List<Object> works_per_month) {
//        this.works_per_month = works_per_month;
//    }

    public List<Object> getWorks_per_dashboard() {
        return works_per_dashboard;
    }

    public void setWorks_per_dashboard(List<Object> works_per_dashboard) {
        this.works_per_dashboard = works_per_dashboard;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
