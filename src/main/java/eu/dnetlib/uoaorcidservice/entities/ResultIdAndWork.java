package eu.dnetlib.uoaorcidservice.entities;

public class ResultIdAndWork {
    private String dashboard = "production_explore";
    String[] pids;
    private String openaireId;
    Object work;

    public String getDashboard() {
        return dashboard;
    }

    public void setDashboard(String dashboard) {
        this.dashboard = dashboard;
    }

    public String[] getPids() {
        return pids;
    }

    public void setPids(String[] pids) {
        this.pids = pids;
    }

    public String getOpenaireId() {
        return openaireId;
    }

    public void setOpenaireId(String openaireId) {
        this.openaireId = openaireId;
    }

    public Object getWork() {
        return work;
    }

    public void setWork(Object work) {
        this.work = work;
    }
}
